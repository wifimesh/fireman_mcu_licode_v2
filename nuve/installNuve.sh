#!/bin/bash

SCRIPT=`pwd`/$0
FILENAME=`basename $SCRIPT`
PATHNAME=`dirname $SCRIPT`
ROOT=$PATHNAME/..
BUILD_DIR=$ROOT/build
CURRENT_DIR=`pwd`
DB_DIR="$BUILD_DIR"/db

cd $PATHNAME

cd nuveAPI

echo [nuve] Installing node_modules for nuve

npm install amqp express mongojs aws-lib log4js node-getopt body-parser
echo [nuve] Done, node_modules installed

cd ../nuveClient/tools

############
# following script will create nuve.js & xmlhttprequest.js in nuve/nuvelClient/dist. They are already compiled.
############
#./compile.sh

echo [nuve] Done, nuve.js compiled

cd $CURRENT_DIR

#!/bin/bash -x

SCRIPT=`pwd`/$0
FILENAME=`basename $SCRIPT`
PATHNAME=`dirname $SCRIPT`
ROOT=$PATHNAME/..
BUILD_DIR=$ROOT/build
CURRENT_DIR=`pwd`
LIB_DIR=$BUILD_DIR/libdeps
PREFIX_DIR=$LIB_DIR/build/


export ERIZO_HOME=$ROOT/erizo

usage()
{
cat << EOF
usage: $0 options

Compile erizo libraries:
- Erizo is the C++ core
- Erizo API is the Javascript layer of Erizo (require Erizo to be compiled)
- Erizo Controller is the node interface

OPTIONS:
   -h      Show this message
   -e      Compile Erizo
   -a      Compile Erizo API
   -c      Install Erizo node modules
EOF
}

pause() {
  read -p "$*"
}

compile_erizo(){
  echo 'Building erizo...'
  cd $ROOT/erizo
  ./generateProject.sh
  ./buildProject.sh
  cd $CURRENT_DIR
}

compile_erizo_api(){
  echo 'Building erizoAPI...'
  cd $ROOT/erizoAPI
  ./build.sh
  cd $CURRENT_DIR
}

compile_erizo
compile_erizo_api

#!/bin/bash -x
SCRIPT=`pwd`/$0
FILENAME=`basename $SCRIPT`
PATHNAME=`dirname $SCRIPT`
ROOT=$PATHNAME/..
BUILD_DIR=$ROOT/build
CURRENT_DIR=`pwd`
DB_DIR="$BUILD_DIR"/db

LIB_DIR=$BUILD_DIR/libdeps
PREFIX_DIR=$LIB_DIR/build/

pause() {
  read -p "$*"
}

parse_arguments(){
  while [ "$1" != "" ]; do
    case $1 in
      "--enable-gpl")
        ENABLE_GPL=true
        ;;
      "--cleanup")
        CLEANUP=true
        ;;
    esac
    shift
  done
}

check_proxy(){
  if [ -z "$http_proxy" ]; then
    echo "No http proxy set, doing nothing"
  else
    echo "http proxy configured, configuring npm"
    npm config set proxy $http_proxy
  fi  

  if [ -z "$https_proxy" ]; then
    echo "No https proxy set, doing nothing"
  else
    echo "https proxy configured, configuring npm"
    npm config set https-proxy $https_proxy
  fi  
}

set_env_path(){
  echo "Update user env with node js path."
  if [ -z "$NODE_JS_HOME" ]; then
    echo "~/.bashrc is updated, please re-login when you run MCU"

    # Do not change the sequence of following 4 lines
    export NODE_JS_HOME=/opt/node-v0.10.2-linux-arm-pi
    echo "export NODE_JS_HOME=/opt/node-v0.10.2-linux-arm-pi" >> ~/.bashrc
    echo "export PATH=$NODE_JS_HOME/bin:$PATH" >> ~/.bashrc
    export PATH=$NODE_JS_HOME/bin:$PATH

  fi
}

install_rasbian_nodejs(){
  echo "Install pre-built nodejs & npm..."
  cd /opt
  tar xfz $ROOT/rasbian_pkgs/node-v0.10.2-linux-arm-pi.tar.gz
  chown -R root:root node-v0.10.2-linux-arm-pi

  echo "Install npm and node-gyp"
  npm install -g npm
  npm install -g node-gyp
}

install_rasbian_mongodb(){
  echo "Install pre-built mongodb"

  adduser --firstuid 100 --ingroup nogroup --shell /etc/false --disabled-password --gecos "" --no-create-home mongodb

  cd /opt
  unzip $ROOT/rasbian_pkgs/mongodb-rpi_20140207.zip
  #chown -R root:root mongodb-rpi
  ln -s /opt/mongodb-rpi/mongo /opt/mongo
  chmod a+x /opt/mongo/bin/*

  mkdir /var/log/mongodb
  chown mongodb:nogroup /var/log/mongodb

  cp /opt/mongodb-rpi/debian/init.d /etc/init.d/mongod
  cp /opt/mongodb-rpi/debian/mongodb.conf /etc/

  ln -s /opt/mongo/bin/mongod /usr/bin/mongod
  ln -s /opt/mongo/bin/mongo /usr/bin/mongo
  ln -s /opt/mongo/bin/mongodump /usr/bin/mongodump
  chmod u+x /etc/init.d/mongod

  update-rc.d mongod defaults
  # daemon fail???
  /etc/init.d/mongod start
}

install_apt_deps(){
  sudo apt-get install -y python-software-properties
  sudo apt-get install -y software-properties-common
  sudo add-apt-repository -y ppa:chris-lea/node.js
  sudo apt-get update
  sudo apt-get install -y git make gcc g++ libssl-dev cmake libglib2.0-dev pkg-config libboost-regex-dev libboost-thread-dev libboost-system-dev liblog4cxx10-dev rabbitmq-server openjdk-6-jre curl libboost-test-dev
  sudo chown -R `whoami` ~/.npm ~/tmp/
}

install_openssl(){
  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
    #curl -O http://www.openssl.org/source/openssl-1.0.1g.tar.gz
    #tar -zxvf openssl-1.0.1g.tar.gz
    cd openssl-1.0.1g
    ./config --prefix=$PREFIX_DIR -fPIC
    make -s V=0
    make install
    cd $CURRENT_DIR
  else
    mkdir -p $LIB_DIR
    install_openssl
  fi
}

install_libnice(){
  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
    #curl -O http://nice.freedesktop.org/releases/libnice-0.1.4.tar.gz
    #tar -zxvf libnice-0.1.4.tar.gz
    cd libnice-0.1.4
    #patch -R ./agent/conncheck.c < $PATHNAME/libnice-014.patch0
    #patch -p1 < $PATHNAME/libnice-014.patch1
    ./configure --prefix=$PREFIX_DIR
    make -s V=0
    make install
    cd $CURRENT_DIR
  else
    mkdir -p $LIB_DIR
    install_libnice
  fi
}

install_opus(){
  [ -d $LIB_DIR ] || mkdir -p $LIB_DIR
  cd $LIB_DIR
  #curl -O http://downloads.xiph.org/releases/opus/opus-1.1.tar.gz
  #tar -zxvf opus-1.1.tar.gz
  cd opus-1.1
  ./configure --prefix=$PREFIX_DIR
  make -s V=0
  make install
  cd $CURRENT_DIR
}

install_libvpx(){
  apt-get install -y checkinstall
  cd $LIB_DIR
  cd libvpx-1.4.0
  ./configure
  make
  sudo checkinstall --pkgname=libvpx --pkgversion="1:$(date +%Y%m%d%H%M)-git" --backup=no --deldoc=yes --fstrans=no --default
}

install_mediadeps(){
  sudo apt-get install -y yasm libx264.

  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
#    curl -O https://www.libav.org/releases/libav-11.1.tar.gz
#    tar -zxvf libav-11.1.tar.gz
    cd libav-11.1
    PKG_CONFIG_PATH=${PREFIX_DIR}/lib/pkgconfig ./configure --prefix=$PREFIX_DIR --enable-shared --enable-gpl --enable-libvpx --enable-libx264 --enable-libopus
    make -s V=0
    make install
    cd $CURRENT_DIR
  else
    mkdir -p $LIB_DIR
    install_mediadeps
  fi

}

install_libsrtp(){
  cd $ROOT/third_party/srtp
  CFLAGS="-fPIC" ./configure --prefix=$PREFIX_DIR
  make -s V=0
  make uninstall
  make install
  cd $CURRENT_DIR
}

populate_mongo(){

  cd $ROOT/nuve

  if ! pgrep mongod; then
    echo [licode] Starting mongodb
    if [ ! -d "$DB_DIR" ]; then
      mkdir -p "$DB_DIR"/db
    fi
    mongod --repair --dbpath $DB_DIR
    mongod --dbpath $DB_DIR --logpath $BUILD_DIR/mongo.log --fork
    sleep 5
  else
    echo [licode] mongodb already running
  fi

  dbURL=`grep "config.nuve.dataBaseURL" $PATHNAME/licode_default.js`

  dbURL=`echo $dbURL| cut -d'"' -f 2`
  dbURL=`echo $dbURL| cut -d'"' -f 1`

  echo [licode] Creating superservice in $dbURL
  mongo $dbURL --eval "db.services.insert({name: 'superService', key: '$RANDOM', rooms: []})"
  SERVID=`mongo $dbURL --quiet --eval "db.services.findOne()._id"`
  SERVKEY=`mongo $dbURL --quiet --eval "db.services.findOne().key"`

  SERVID=`echo $SERVID| cut -d'"' -f 2`
  SERVID=`echo $SERVID| cut -d'"' -f 1`

  if [ -f "$BUILD_DIR/mongo.log" ]; then
    echo "Mongo Logs: "
    cat $BUILD_DIR/mongo.log
  fi

  echo [licode] SuperService ID $SERVID
  echo [licode] SuperService KEY $SERVKEY
  cd $BUILD_DIR
  replacement=s/_auto_generated_ID_/${SERVID}/
  sed $replacement $PATHNAME/licode_default.js > $BUILD_DIR/licode_1.js
  replacement=s/_auto_generated_KEY_/${SERVKEY}/
  sed $replacement $BUILD_DIR/licode_1.js > $ROOT/licode_config.js
  rm $BUILD_DIR/licode_1.js
}


cleanup(){  
  if [ -d $LIB_DIR ]; then
    cd $LIB_DIR
    rm -r libnice*
    rm -r libav*
    rm -r openssl*
    cd $CURRENT_DIR
  fi
}

parse_arguments $*

mkdir -p $PREFIX_DIR

#pause "Installing deps via apt-get... [press Enter]"
set_env_path

install_rasbian_nodejs
install_apt_deps

install_rasbian_mongodb
populate_mongo

#check_proxy

echo "Installing openssl library..."
install_openssl

echo "Installing libnice library..."
install_libnice

echo "Installing libsrtp library..."
install_libsrtp

echo "Installing opus library..."
install_opus

echo "Installing media library..."
install_libvpx
install_mediadeps

#if [ "$CLEANUP" = "true" ]; then
#  echo "Cleaning up..."
#  cleanup
#fi

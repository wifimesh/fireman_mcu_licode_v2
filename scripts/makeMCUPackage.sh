#!/bin/bash -x

SCRIPT=`pwd`/$0
FILENAME=`basename $SCRIPT`
PATHNAME=`dirname $SCRIPT`
ROOT=$PATHNAME/..
RELEASE_DIR=$ROOT/release/nomad_mcu
BACKUP_DIR=$ROOT/release/nomad_mcu_bk

BUILD_DIR1=build/libdeps/build
BUILD_DIR2=erizo/build/erizo
BUILD_DIR3=erizoAPI/build/Release


# 1. To check the existence of running files.
if [ ! -d "$ROOT/$BUILD_DIR1" ]; then
    echo "System Dependent Library($BUILD_DIR1) is not installed"
    exit
fi

if [ ! -f "$ROOT/$BUILD_DIR2/liberizo.so" ]; then
    echo "Erizo Library is not installed"
    exit
fi

if [ ! -f "$ROOT/$BUILD_DIR3/addon.node" ]; then
    echo "Erizo API is not installed"
    exit
fi


# 2. Copy to Release directory.
# <MCU ROOT>/release/nomad_mcu

if [ ! -d "$RELEASE_DIR" ]; then
    echo "Create new release directory"
    mkdir -p "$RELEASE_DIR"
else
    echo "Move previous release to backup directory"
    rm -rf $BACKUP_DIR
    mv $RELEASE_DIR $BACKUP_DIR
    mkdir -p $RELEASE_DIR
fi

cd $RELEASE_DIR

mkdir -p $BUILD_DIR1
cp -r $ROOT/$BUILD_DIR1/* $BUILD_DIR1

mkdir -p $BUILD_DIR2
cp $ROOT/$BUILD_DIR2/liberizo.so $BUILD_DIR2/liberizo.so

mkdir -p $BUILD_DIR3
cp $ROOT/$BUILD_DIR3/addon.node $BUILD_DIR3/addon.node

cp -r $ROOT/erizo_controller .

mkdir -p nuve/nuveClient/dist
cp -r $ROOT/nuve/nuveClient/dist nuve/nuveClient/
cp -r $ROOT/nuve/nuveAPI nuve/
cp $ROOT/nuve/*.sh nuve/
cp $ROOT/nuve/*.json nuve/

cp -r $ROOT/*.js .
cp -r $ROOT/*.json .
cp -r $ROOT/cert .
cp -r $ROOT/extras .
cp -r $ROOT/node_modules .
cp -r $ROOT/scripts .


#!/bin/bash

echo [erizo_controller] Installing node_modules for erizo_controller

npm install amqp socket.io@0.9.16 log4js node-getopt

echo [erizo_controller] Done, node_modules installed


#############
# Following two scrips are used for creating erizo.js & erizofc.js in ./erizoClient/dist. They are already created.
# So, those are commented out
#############
#cd ./erizoClient/tools
#./compile.sh
#./compilefc.sh

echo [erizo_controller] Done, erizo.js compiled
